package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class NacosConfigController {

    @Autowired
    private ConfigurableApplicationContext applicationContext;

    @Value("${config.appName}")
    private String appName;

    @RequestMapping("test-config1")
    public String testCofig1(){
        // 从Nacos中获取指定键的位置
        return applicationContext.getEnvironment().getProperty("config.appName");
    }

    @RequestMapping("test-config2")
    public String testCofig2(){
        // 从Nacos中获取指定键的位置
        return appName;
    }
}
