package com.example.service;

import com.example.domain.Product;

public interface ProductService {

    /**
     * 通过ID查询产品
     * @param pid 产品ID
     * @return
     */
    Product findById(Integer pid);

    /**
     * 减少库存并保存
     * @param pid 产品ID
     * @param number 减少数量
     */
    void reduceInventory(Integer pid, Integer number);
}
