package com.example.service.impl;


import com.example.dao.ProductDao;
import com.example.domain.Product;
import com.example.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductDao productDao;
    @Override
    public Product findById(Integer pid) {
        return productDao.findById(pid).orElseThrow(()->new RuntimeException("数据不存在"));
    }

    @Override
    @Transactional
    public void reduceInventory(Integer pid, Integer number) {
        // 通过pid查询数据
        Product product = productDao.findById(pid).orElseThrow(()->new RuntimeException("数据不存在"));
        // 扣库存
        product.setStock(product.getStock()-number);
        // 模拟异常
        int i = 1/0;
        // 保存到数据库
        productDao.save(product);
    }
}
